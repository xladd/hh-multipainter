import { readable, writable } from 'svelte/store';

export const User = writable({ id: 0, name: '', loggedIn: false });
export const CUBE_SIZE = readable(0.1);
export const MAPLENGTH = writable(50);
export const MAPWIDTH = writable(75);
export const TILEHEIGHT = writable(5);

export const hoverPos = writable([0, 0, 0]);
export const UIselectedColor = writable('black');
export const UIphysTurnOn = writable(false);
