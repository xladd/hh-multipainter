import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export async function POST({ request, cookies }) {
	const { description } = await request.json();

	const userid = cookies.get('userid');
	const ANS = { id: 1, name: description, loggedIn: true };

	return json(ANS, { status: 201 });
}
