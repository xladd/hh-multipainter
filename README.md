# hh-multipainter

<img src="./Screenshot%202024-05-08%20001301.png" alt="proof"/>

### Dev

Running Locally:

`npm run dev -- --open`

### Building

To create a production version of the app and start a server on localhost:3005:

`npm run build`
`npm run prodServer`


### Based on
- https://github.com/suhaildawood/SvelteKit-integrated-WebSocket
- https://threlte.xyz/docs/reference/rapier/getting-started